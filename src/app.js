// import dependencies
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

const app = express(); // create your express app

// make app use dependencies
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cors());

app.listen(process.env.PORT || 8081);

//import MongoDB
const mongo = require("mongodb");
const MongoClient = mongo.MongoClient;
const uri =
  "mongodb+srv://hospitaal:Hospitaal123@hospitaalcluster-se96b.gcp.mongodb.net/test?retryWrites=true&w=majority&authSource=admin";

var client;

var mongoClient = new MongoClient(uri, {
  reconnectTries: Number.MAX_VALUE,
  autoReconnect: true,
  useNewUrlParser: true
});

mongoClient.connect((err, db) => {
  if (err != null) {
    console.log(err);
    return;
  }
  client = db;
});

//GET Actions
app.get("/actions", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("actions");
  collection.find().toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Actions by id
app.get("/acties", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("actions");
  var depIDs = req.query.id;
  var ids = [];

  for (const key in depIDs) {
    if (depIDs.hasOwnProperty(key)) {
      if (Array.isArray(depIDs)) {
        var element = depIDs[key];
      } else {
        var element = depIDs;
      }
      var nummer = parseInt(element, 10);
      ids.push(nummer);
    }
  }

  collection.find({ id: { $in: ids } }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//POST (Add) Actions
app.post("/actions", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("actions");
  var action = req.body.action; // parse the data from the request's body
  collection.insertOne(
    {
      id: action.id,
      type: action.type,
      description: action.description,
      date: action.date,
      done: action.done
    },
    function(err, results) {
      if (err) {
        console.log(err);
        res.send("");
        return;
      }
      res.send(results.ops[0]); // returns the new document
    }
  );
});

//DELETE Actions
app.post("/deleteAction", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("actions");
  // remove document by its unique _id
  collection.removeOne({ _id: mongo.ObjectID(req.body.actionId) }, function(
    err,
    results
  ) {
    if (err) {
      console.log(err);
      res.send("");
      return;
    }
    res.send(); // return
  });
});

//UPDATE Actions
app.put("/actions", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("actions");
  collection.updateOne(
    { _id: mongo.ObjectID(req.body.action._id) },
    {
      $set: {
        type: req.body.action.type,
        description: req.body.action.description,
        date: req.body.action.date,
        done: req.body.action.done
      }
    }
  );
  res.send(); // return
});

//GET Departments
app.get("/departments", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("departments");
  collection.find().toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Departments by id
app.get("/departementen", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("departments");
  var depID = req.query.id;
  var nummer = parseInt(depID, 10);
  collection.find({ id: nummer }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Rooms
app.get("/rooms", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("rooms");
  collection.find().toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Room by DepartmentId
app.get("/roomen", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("rooms");
  var depID = req.query.department;
  var nummer = parseInt(depID, 10);
  collection.find({ department: nummer }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Room by Patient
app.get("/roomkes", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("rooms");
  var patientId = req.query.patientId;
  var nummer = parseInt(patientId, 10);
  collection.find({ patientId: nummer }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//GET Setting by name
app.get("/settings", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("settings");
  var name = req.query.name;

  collection.find({ name: name }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//UPDATE Setting
app.put("/settings", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("settings");
  collection.updateOne(
    { _id: mongo.ObjectID(req.body.setting._id) },
    {
      $set: {
        value: req.body.setting.value
      }
    }
  );
  res.send(); // return
});

//GET Patient by id
app.get("/patients", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("users");
  var ID = req.query.id;
  var nummer = parseInt(ID, 10);
  collection.find({ id: nummer }).toArray(function(err, results) {
    if (err) {
      console.log(err);
      res.send([]);
      return;
    }

    res.send(results);
  });
});

//UPDATE Patient
app.put("/patients", (req, res) => {
  const collection = client.db("HospitaalDatabase").collection("users");
  collection.updateOne(
    { _id: mongo.ObjectID(req.body.patient._id) },
    {
      $set: {
        vegetarian: req.body.patient.vegetarian,
        actions: req.body.patient.actions,
        remarks: req.body.patient.remarks
      }
    }
  );
  res.send(); // return
});
